package com.johnlewis.betechtest.products.response;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ProductTest {

    @Test
    void shouldBuildProduct() {
        String productId = "productId";
        String title = "title";
        Price price = new Price("10.00", "5.00", "2.00");
        ColorSwatch colorSwatch = new ColorSwatch("Blue", "skuId", null);
        List<ColorSwatch> colorSwatches = List.of(colorSwatch);

        Product product = new Product();
        product.setProductId(productId);
        product.setTitle(title);
        product.setColorSwatches(colorSwatches);
        product.setPrice(price);

        assertEquals(productId, product.getProductId());
        assertEquals(title, product.getTitle());
        assertEquals(colorSwatches, product.getColorSwatches());
        assertEquals(price, product.getPrice());
    }
}
