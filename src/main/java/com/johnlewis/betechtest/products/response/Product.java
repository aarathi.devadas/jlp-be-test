package com.johnlewis.betechtest.products.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Product {
    private String productId;
    private String title;
    private String nowPrice;
    private Price price;
    private String priceLabel;
    private List<ColorSwatch> colorSwatches;
}
