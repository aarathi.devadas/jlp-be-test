package com.johnlewis.betechtest.products.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.johnlewis.betechtest.products.exception.ServiceException;
import com.johnlewis.betechtest.products.response.Product;
import com.johnlewis.betechtest.products.service.DressesService;

class ProductsControllerTest {

    private final DressesService dressesService = mock(DressesService.class);
    private final ProductsController productsController = new ProductsController(dressesService);

    @AfterEach
    void after(){
        verifyNoMoreInteractions(dressesService);
    }

    @Test
    void shouldTest(){
        ResponseEntity<List<Product>> result = productsController.getDiscountDresses();
        verify(dressesService).getDresses();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
    }
    
    @Test
    public void getDiscountedDressTest() throws ServiceException {
    	ResponseEntity<List<Product>> result = productsController.getDressesInDiscount("ShowWasNow");
    	verify(dressesService).getDiscountedDresses("ShowWasNow");
    	assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
    }
    
    @Test()
    public void getDiscountedDressWithInvalidLabelTest() throws ServiceException {

    	assertThrows(ServiceException.class,()->{productsController.getDressesInDiscount("INVALID-LABEL");});
    }
}