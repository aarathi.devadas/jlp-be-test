package com.johnlewis.betechtest.products.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.johnlewis.betechtest.products.exception.ServiceException;
import com.johnlewis.betechtest.products.response.ColorSwatchDto;
import com.johnlewis.betechtest.products.response.Display;
import com.johnlewis.betechtest.products.response.Product;
import com.johnlewis.betechtest.products.response.ProductDto;
import com.johnlewis.betechtest.products.response.ProductResponse;
import com.johnlewis.betechtest.products.response.ReductionHistoryDto;
import com.johnlewis.betechtest.products.response.VariantPriceRangeDto;

public class DressesServiceTest {
    private static final String API_KEY = "xxxxxxxxx";

    private final RestTemplate restTemplate = mock(RestTemplate.class);
    private final DressesService dressesService = new DressesService(restTemplate, API_KEY);

    @Test
    void shouldReturnEmptyListWhenExceptionThrown() {

        when(restTemplate.exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class))).thenThrow(RestClientException.class);

        List<Product> products = dressesService.getDresses();
        assertNotNull(products);
        assertEquals(0, products.size());
    }

    @Test
    void shouldReturnEmptyListAsEmptyResponse() {

        ProductResponse productResponse = new ProductResponse();
        ResponseEntity<ProductResponse> responseEntity = new ResponseEntity<>(productResponse, HttpStatus.OK);
        when(restTemplate.exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class))).thenReturn(responseEntity);

        List<Product> products = dressesService.getDresses();
        assertNotNull(products);
        assertEquals(0, products.size());
    }

    @Test
    void shouldReturnEmptyListAsProductListEmpty() {
        ProductResponse productResponse = new ProductResponse();
        productResponse.setProducts(new ArrayList<>());
        ResponseEntity<ProductResponse> responseEntity = new ResponseEntity<>(productResponse, HttpStatus.OK);
        when(restTemplate.exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class))).thenReturn(responseEntity);

        List<Product> products = dressesService.getDresses();
        assertNotNull(products);
        assertEquals(0, products.size());
    }

    @Test
    void shouldReturnProducts() {
        ProductResponse productResponse = new ProductResponse();
        productResponse.setProducts(getProductDtos());
        ResponseEntity<ProductResponse> responseEntity = new ResponseEntity<>(productResponse, HttpStatus.OK);
        when(restTemplate.exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class))).thenReturn(responseEntity);

        List<Product> products = dressesService.getDresses();
        assertNotNull(products);
        assertEquals(1, products.size());
    }
    
    @Test
    public void getDiscountedProductsWithLabelShowWasNow() throws ServiceException, JsonParseException, JsonMappingException, IOException {
    	 ProductResponse productResponse = new ProductResponse();
         productResponse.setProducts(getProducts());
         ResponseEntity<ProductResponse> responseEntity = new ResponseEntity<>(productResponse, HttpStatus.OK);
         when(restTemplate.exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class))).thenReturn(responseEntity);

         List<Product> products = dressesService.getDiscountedDresses("ShowWasNow");
         assertNotNull(products);
         assertEquals(1, products.size());
    	
    }

    @Test
    public void getDiscountedProductsWithLabelShowWasThenNow() throws ServiceException, JsonParseException, JsonMappingException, IOException {
    	 ProductResponse productResponse = new ProductResponse();
         productResponse.setProducts(getProducts());
         ResponseEntity<ProductResponse> responseEntity = new ResponseEntity<>(productResponse, HttpStatus.OK);
         when(restTemplate.exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class))).thenReturn(responseEntity);

         List<Product> products = dressesService.getDiscountedDresses("ShowWasThenNow");
         assertNotNull(products);
         assertEquals(1, products.size());
    	
    }
    
   
    
   @Test
    public void getDiscountedProductsWithLabelShowPercDscount() throws ServiceException, JsonParseException, JsonMappingException, IOException {
    	 ProductResponse productResponse = new ProductResponse();
         productResponse.setProducts(getProducts());
         ResponseEntity<ProductResponse> responseEntity = new ResponseEntity<>(productResponse, HttpStatus.OK);
         when(restTemplate.exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class))).thenReturn(responseEntity);

         List<Product> products = dressesService.getDiscountedDresses("ShowPercDscount");
         System.out.println("products ::"+products.size());
         assertNotNull(products);
         assertEquals(2, products.size());
    	
    }
    @Test
    public void getDiscountedProductsWithEmptyLabel() throws ServiceException, JsonParseException, JsonMappingException, IOException {
    	 ProductResponse productResponse = new ProductResponse();
         productResponse.setProducts(getProducts());
         ResponseEntity<ProductResponse> responseEntity = new ResponseEntity<>(productResponse, HttpStatus.OK);
         when(restTemplate.exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class))).thenReturn(responseEntity);

         List<Product> products = dressesService.getDiscountedDresses("");
         assertNotNull(products);
         assertEquals(1, products.size());
    	
    }

    
    private List<ProductDto> getProductDtos() {
        String productId = "productId";
        String title = "title";
        VariantPriceRangeDto variantPriceRangeDto = new VariantPriceRangeDto();
        variantPriceRangeDto.setDisplay(new Display("10.00", "5.00"));
        ReductionHistoryDto reductionHistory = new ReductionHistoryDto();
        reductionHistory.setDisplay(new Display("15.00", "10.00"));
        reductionHistory.setChronology(0);
        variantPriceRangeDto.setReductionHistory(List.of(reductionHistory));
        ColorSwatchDto colorSwatchDto = new ColorSwatchDto();
        colorSwatchDto.setBasicColor("Blue");
        colorSwatchDto.setSkuId("skuId");
        List<ColorSwatchDto> colorSwatchDtos = List.of(colorSwatchDto);

        ProductDto productDto = new ProductDto();
        productDto.setProductId(productId);
        productDto.setTitle(title);
        productDto.setColorSwatches(colorSwatchDtos);
        productDto.setVariantPriceRange(variantPriceRangeDto);
        return List.of(productDto);
    }
    
    private List<ProductDto> getProducts() {
        List<ProductDto> products = new ArrayList<ProductDto>();
        products.add(getNoDiscountProduct());
        products.add(getWasNowDiscountProduct());
        products.add(getWasThenNowDiscountProduct());
        
        
        return products;
    }
    
    private ProductDto getNoDiscountProduct() {
    	VariantPriceRangeDto variantPriceRangeDto = new VariantPriceRangeDto();
        variantPriceRangeDto.setDisplay(new Display("10.00", "5.00"));
       
        ColorSwatchDto colorSwatchDto = new ColorSwatchDto();
        colorSwatchDto.setBasicColor("Blue");
        colorSwatchDto.setSkuId("skuId");
        List<ColorSwatchDto> colorSwatchDtos = List.of(colorSwatchDto);

        ProductDto productDto = new ProductDto();
        productDto.setProductId("5520412");
        productDto.setTitle("John Lewis & Partners School Belted Gingham Checked Summer Dress");
        productDto.setColorSwatches(colorSwatchDtos);
        productDto.setVariantPriceRange(variantPriceRangeDto);
        
        return productDto;
    }
    
    private ProductDto getWasNowDiscountProduct() {
    	VariantPriceRangeDto variantPriceRangeDto = new VariantPriceRangeDto();
        variantPriceRangeDto.setDisplay(new Display("10.00", "5.00"));
        ReductionHistoryDto reductionHistory = new ReductionHistoryDto();
        reductionHistory.setDisplay(new Display("15.00", "10.00"));
        reductionHistory.setChronology(0);
        variantPriceRangeDto.setReductionHistory(List.of(reductionHistory));
        
        ColorSwatchDto colorSwatchDto = new ColorSwatchDto();
        colorSwatchDto.setBasicColor("Blue");
        colorSwatchDto.setSkuId("skuId");
        
        List<ColorSwatchDto> colorSwatchDtos = List.of(colorSwatchDto);

        ProductDto productDto = new ProductDto();
        productDto.setProductId("5520412");
        productDto.setTitle("John Lewis & Partners School Belted Gingham Checked Summer Dress");
        productDto.setColorSwatches(colorSwatchDtos);
        productDto.setVariantPriceRange(variantPriceRangeDto);
        
        return productDto;
    }
    
    private ProductDto getWasThenNowDiscountProduct() {
    	VariantPriceRangeDto variantPriceRangeDto = new VariantPriceRangeDto();
        variantPriceRangeDto.setDisplay(new Display("20.00", "5.00"));
        ReductionHistoryDto reductionHistory = new ReductionHistoryDto();
        reductionHistory.setDisplay(new Display("50.00", "10.00"));
        reductionHistory.setChronology(0);
        
        List<ReductionHistoryDto> reductionHistories = new ArrayList<ReductionHistoryDto>();
        ReductionHistoryDto reductionHistory1 = new ReductionHistoryDto();
        reductionHistory1.setDisplay(new Display("30.00", "10.00"));
        reductionHistory1.setChronology(1);
        reductionHistories.add(reductionHistory);
        reductionHistories.add(reductionHistory1);
        variantPriceRangeDto.setReductionHistory(reductionHistories);
        
        ColorSwatchDto colorSwatchDto = new ColorSwatchDto();
        colorSwatchDto.setBasicColor("Blue");
        colorSwatchDto.setSkuId("skuId");
        
        List<ColorSwatchDto> colorSwatchDtos = List.of(colorSwatchDto);

        ProductDto productDto = new ProductDto();
        productDto.setProductId("5520412");
        productDto.setTitle("John Lewis & Partners School Belted Gingham Checked Summer Dress");
        productDto.setColorSwatches(colorSwatchDtos);
        productDto.setVariantPriceRange(variantPriceRangeDto);
        
        return productDto;
    }
/*    private List<Product> getDiscountedProductList(){
    	List<Product> products = new ArrayList();
    	Product product = new Product();
    	Price price = new Price(null, null, "£14.00");
    	ColorSwatch colorSwatch = new ColorSwatch("Yellow", "239018750" , null);
    	ColorSwatch colorSwatch1 = new ColorSwatch("Blue", "239018712" , null);
    	ColorSwatch colorSwatch2 = new ColorSwatch("Yellow", "239018662" , null);
    	List<ColorSwatch> colorSwatchList = new ArrayList<ColorSwatch>();
    	colorSwatchList.add(colorSwatch);
    	colorSwatchList.add(colorSwatch1);
    	colorSwatchList.add(colorSwatch2);
    	
    	product.setProductId("5520412");
    	product.setTitle("John Lewis & Partners School Belted Gingham Checked Summer Dress");
    	product.setPrice(price);
    	product.setColorSwatch(colorSwatchList);
    	
    	
    	Product product2 = new Product();
    	Price price2 = new Price("£28.00", null, "£14.00");
    	ColorSwatch colorSwatch3 = new ColorSwatch("Yellow", "239018750" , null);
    	ColorSwatch colorSwatch4 = new ColorSwatch("Blue", "239018712" , null);
    	ColorSwatch colorSwatch5 = new ColorSwatch("Green", "239018662" , null);
    	colorSwatchList = new ArrayList<ColorSwatch>();
    	colorSwatchList.add(colorSwatch3);
    	colorSwatchList.add(colorSwatch4);
    	colorSwatchList.add(colorSwatch5);
    	
    	product2.setProductId("765160");
    	product2.setTitle("John Lewis & Partners Gingham Cotton School Summer Dress");
    	product2.setPrice(price);
    	product2.setColorSwatch(colorSwatchList);
    	
    	
    	Product product3 = new Product();
    	Price price3 = new Price("£28.00", "£25.00", "£14.00");
    	ColorSwatch colorSwatch6 = new ColorSwatch("Yellow", "239018750" , null);
    	ColorSwatch colorSwatch7 = new ColorSwatch("Black", "239018712" , null);
    	ColorSwatch colorSwatch8 = new ColorSwatch("Green", "239018662" , null);
    	colorSwatchList = new ArrayList<ColorSwatch>();
    	colorSwatchList.add(colorSwatch6);
    	colorSwatchList.add(colorSwatch7);
    	colorSwatchList.add(colorSwatch8);
    	
    	product3.setProductId("5727625");
    	product3.setTitle("ANYDAY John Lewis & Partners Kids' Tiered Jersey Dress");
    	product3.setPrice(price);
    	product3.setColorSwatch(colorSwatchList);
    	
    	products.add(product);
    	products.add(product2);
    	products.add(product3);
    	
    }
*/
}
