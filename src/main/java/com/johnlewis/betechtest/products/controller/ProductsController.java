package com.johnlewis.betechtest.products.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.johnlewis.betechtest.products.exception.ServiceException;
import com.johnlewis.betechtest.products.response.Product;
import com.johnlewis.betechtest.products.service.DressesService;
import com.johnlewis.betechtest.products.utils.Constants;
import com.johnlewis.betechtest.products.utils.Utils;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/products")
public class ProductsController {

	private final DressesService dressesService;

	@Autowired
	ProductsController(DressesService dressesService) {
		this.dressesService = dressesService;
	}

	@GetMapping(value = "/dresses", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Endpoint to obtain dress information")
	public ResponseEntity<List<Product>> getDiscountDresses() {
		return ResponseEntity.ok(this.dressesService.getDresses());
	}

	@GetMapping(value = "/dresses/discounts")
	@ApiOperation(value = "Endpoint to retrieve the discounted products")
	public ResponseEntity<List<Product>> getDressesInDiscount(@RequestParam(required = false) String labelType)
			throws ServiceException {
		validateLabelType(labelType);
		return ResponseEntity.ok(this.dressesService.getDiscountedDresses(labelType));
	}

	private void validateLabelType(String labelType) throws ServiceException {
		if (Utils.isNotNull(labelType) && !Utils.isEqual(labelType, Constants.EMPTY_STR)
				&& !((Utils.isEqual(labelType, Constants.SHOW_PERC_DISCOUNT))
						|| (Utils.isEqual(labelType, Constants.SHOW_WAS_NOW))
						|| (Utils.isEqual(labelType, Constants.SHOW_WAS_THEN_NOW)))) {
			throw new ServiceException("400", "Invalid LabelType");
		}
	}

}
