package com.johnlewis.betechtest.products.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.johnlewis.betechtest.products.exception.ServiceException;
import com.johnlewis.betechtest.products.response.ErrorResponse;

@ControllerAdvice
public class ExceptionController {

	@ExceptionHandler(ServiceException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ResponseEntity<ErrorResponse> serviceException(ServiceException exception) {
		ErrorResponse error = new ErrorResponse(exception.getErrorCode(), exception.getErrorDesc());

		return new ResponseEntity<ErrorResponse>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
