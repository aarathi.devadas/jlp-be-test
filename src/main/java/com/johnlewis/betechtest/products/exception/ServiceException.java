package com.johnlewis.betechtest.products.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class ServiceException extends Exception {

	private String errorCode;
	private String errorDesc;
	
}
