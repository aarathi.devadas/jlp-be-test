package com.johnlewis.betechtest.products.service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.johnlewis.betechtest.products.exception.ServiceException;
import com.johnlewis.betechtest.products.mapper.ProductMapper;
import com.johnlewis.betechtest.products.response.ColorSwatch;
import com.johnlewis.betechtest.products.response.Product;
import com.johnlewis.betechtest.products.response.ProductResponse;
import com.johnlewis.betechtest.products.utils.Constants;
import com.johnlewis.betechtest.products.utils.Utils;

@Service
public class DressesService {
	private final String johnLewisDressApi;
	private final RestTemplate restTemplate;
	private final ProductMapper productMapper;

	@Autowired
	DressesService(RestTemplate restTemplate, @Value("${jl.api.key}") String apiKey) {
		this.restTemplate = restTemplate;
		this.productMapper = ProductMapper.INSTANCE;
		this.johnLewisDressApi = "https://api.johnlewis.com/search/api/rest/v2/catalog/products/search/keyword?q=dresses&key="
				+ apiKey;
	}

	public List<Product> getDresses() {
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
			headers.set("User-Agent", "Back-end tech test");
			HttpEntity<String> entity = new HttpEntity<>(headers);
			ProductResponse productResponse = restTemplate
					.exchange(johnLewisDressApi, HttpMethod.GET, entity, ProductResponse.class).getBody();
			return productResponse != null && productResponse.getProducts() != null
					? productResponse.getProducts().stream().map(productMapper::toProduct).collect(Collectors.toList())
					: new ArrayList<>();
		} catch (Exception e) {
			System.out.println(e);
		}
		return new ArrayList<>();
	}

	public List<Product> getDiscountedDresses(String labelType) throws ServiceException {
		List<Product> discountedProducts = null;
		List<Product> products = null;
		try {
			if (Utils.isNullOrEmpty(labelType))
				labelType = Constants.DEFAULT_LABEL_TYPE;
			products = getDresses();
			discountedProducts = getDiscountedProducts(products, labelType);
		} catch (Exception e) {
			System.out.println(e);
			throw new ServiceException("500", "Unable to retrieve the products");
		}

		return discountedProducts;
	}

	private List<Product> getDiscountedProducts(List<Product> products, String labelType) {
		List<Product> discountedProducts = null;
		List<Product> discountedDress = new ArrayList();
		Hashtable<String, String> colorHashTable = mapRGBColor();

		if (isLabelShowWasNow(labelType)) {
			discountedProducts = products.stream().filter(t -> isWasNowProduct(t)).collect(Collectors.toList());
		} else if (isLabelShowWasThenNow(labelType)) {
			discountedProducts = products.stream().filter(t -> isWasThenNowProduct(t)).collect(Collectors.toList());
		} else if (isLabelShowDiscoutedPercentage(labelType)) {
			discountedProducts = products.stream().filter(t -> isProductInDiscount(t)).collect(Collectors.toList());
		}

		discountedProducts.stream().forEach(discountProduct -> {
			Product product = new Product();
			product.setProductId(discountProduct.getProductId());
			product.setTitle(discountProduct.getTitle());
			product.setPriceLabel(setPriceLabel(discountProduct, labelType));
			product.setNowPrice(setNowPrice(discountProduct));
			if (Utils.isNotNull(discountProduct.getColorSwatches())) {
				List<ColorSwatch> colorSwatches = new ArrayList<>();
				discountProduct.getColorSwatches().stream().forEach(colorSwatch -> {
					ColorSwatch color = new ColorSwatch(colorSwatch.getColor(), colorSwatch.getSkuId(),
							(Utils.isNotNull(colorSwatch) && Utils.isNotNull(colorSwatch.getColor()))
									? mapRGBColor().get(colorSwatch.getColor().toUpperCase())
									: Constants.EMPTY_STR);

					colorSwatches.add(color);

				});
				product.setColorSwatches(colorSwatches);
			}
			discountedDress.add(product);
		});
		return discountedDress;

	}

	private String setNowPrice(Product product) {
		StringBuilder builder = new StringBuilder();
		builder.append(Constants.POUND);
		BigDecimal now = (BigDecimal.valueOf(Double.parseDouble(getPrice(product.getPrice().getNow()))));
		builder.append((now.intValue() > 10) ? now.intValue() : now);
		return builder.toString();
	}

	private boolean isLabelShowWasNow(String labelType) {
		return Utils.isEqual(labelType, Constants.SHOW_WAS_NOW);
	}

	private boolean isLabelShowWasThenNow(String labelType) {
		return Utils.isEqual(labelType, Constants.SHOW_WAS_THEN_NOW);
	}

	private boolean isLabelShowDiscoutedPercentage(String labelType) {
		return Utils.isEqual(labelType, Constants.SHOW_PERC_DISCOUNT);
	}

	private boolean isWasThenNowProduct(Product product) {
		return (Utils.isNotNull(product) && Utils.isNotNull(product.getPrice())
				&& Utils.isNotNullAndEmpty(product.getPrice().getWas())
				&& Utils.isNotNullAndEmpty(product.getPrice().getThen())
				&& Utils.isNotNullAndEmpty(product.getPrice().getNow()));
	}

	private boolean isWasNowProduct(Product product) {
		return (Utils.isNotNull(product) && Utils.isNotNull(product.getPrice())
				&& Utils.isNotNullAndEmpty(product.getPrice().getWas())
				&& Utils.isNotNullAndEmpty(product.getPrice().getNow())
				&& Utils.isNullOrEmpty(product.getPrice().getThen()));

	}

	private boolean isProductInDiscount(Product product) {
		return (Utils.isNotNull(product) && Utils.isNotNull(product.getPrice())
				&& Utils.isNotNullAndEmpty(product.getPrice().getNow())
				&& (Utils.isNotNullAndEmpty(product.getPrice().getThen()))
				|| Utils.isNotNullAndEmpty(product.getPrice().getWas()));
	}

	private Hashtable<String, String> mapRGBColor() {
		Hashtable<String, String> colorHashTable = new Hashtable<String, String>();
		colorHashTable.put("BLUE", "#0000FF");
		colorHashTable.put("BLACK", "#000000");
		colorHashTable.put("RED", "#FF0000");
		colorHashTable.put("GREEN", "#00FF00");
		colorHashTable.put("PURPLE", "#800080");
		colorHashTable.put("WHITE", "#FFFFFF");
		colorHashTable.put("YELLOW", "#FFFF00");
		colorHashTable.put("GREY", "#808080");
		colorHashTable.put("BROWN", "##964B00");
		return colorHashTable;
	}

	private String setPriceLabel(Product product, String labelType) {
		StringBuilder builder = new StringBuilder();
		if (Utils.isNotNull(labelType) && Utils.isEqual(labelType, Constants.SHOW_PERC_DISCOUNT)) {
			builder.append(getDiscount(product));
			builder.append("% off - now ");
			builder.append(product.getPrice().getNow());
		} else if (Utils.isNotNull(labelType) && Utils.isEqual(labelType, Constants.SHOW_WAS_NOW)) {
			builder.append("Was ");
			builder.append(product.getPrice().getWas());
			builder.append(",now ");
			builder.append(product.getPrice().getNow());
		} else if (Utils.isNotNull(labelType) && Utils.isEqual(labelType, Constants.SHOW_WAS_THEN_NOW)) {

			builder.append("Was ");
			builder.append(product.getPrice().getWas());
			builder.append(",then ");
			builder.append(product.getPrice().getThen());
			builder.append(",now ");
			builder.append(product.getPrice().getNow());
		}
		return builder.toString();
	}

	private int getDiscount(Product product) {
		BigDecimal was = (BigDecimal.valueOf(Double.parseDouble(getPrice(product.getPrice().getWas()))));
		BigDecimal now = (BigDecimal.valueOf(Double.parseDouble(getPrice(product.getPrice().getNow()))));
		return (((was.subtract(now)).multiply(new BigDecimal(100))).divide(was, new MathContext(2, RoundingMode.FLOOR)))
				.intValue();

	}

	private String getPrice(String priceTag) {
		return priceTag.replaceAll(Constants.POUND, Constants.EMPTY_STR);
	}

}
