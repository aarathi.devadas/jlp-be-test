package com.johnlewis.betechtest.products.utils;

public class Utils {

	public static boolean isNotNull(String value) {
		return (null != value && value.trim().length() > Constants.INT_ZERO);
	}
	
	public static boolean isNull (Object input) {
		return (null == input);
	}
	
	public static boolean isNullOrEmpty (String input) {
		return ((null == input) || (input.trim().isEmpty()));
	}
	
	public static boolean isNotNullAndEmpty(String input) {
		return ((isNotNull(input) && isNot(isEqual(input, Constants.EMPTY_STR))));
	}

	public static boolean isEqual(String actualValue, String compareValue) {
		return ((isNotNull(actualValue) && isNotNull(compareValue) && actualValue.equals(compareValue)));
	}
	
	public static boolean isNotNull(Object value) {
		return (null != value);
	}
	
	public static boolean isNot(boolean input) {
		return !(input);
	}
	
	
}

