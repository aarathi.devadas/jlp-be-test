package com.johnlewis.betechtest.products.controller;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.http.ResponseEntity;

import com.johnlewis.betechtest.products.exception.ServiceException;
import com.johnlewis.betechtest.products.response.ErrorResponse;

public class ExceptionControllerTest {


    ExceptionController exceptionController = new ExceptionController();
    
	@Test
	public void serviceExceptionTest() {
		ServiceException exception = new ServiceException("400","Invalid input");
		 ResponseEntity<ErrorResponse> errorResponse = exceptionController.serviceException(exception);
		assertNotNull(errorResponse);
	}
}
