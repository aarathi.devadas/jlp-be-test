package com.johnlewis.betechtest.products.mapper;

import com.johnlewis.betechtest.products.response.ColorSwatch;
import com.johnlewis.betechtest.products.response.ColorSwatchDto;
import com.johnlewis.betechtest.products.response.Display;
import com.johnlewis.betechtest.products.response.Price;
import com.johnlewis.betechtest.products.response.Product;
import com.johnlewis.betechtest.products.response.ProductDto;
import com.johnlewis.betechtest.products.response.ReductionHistoryDto;
import com.johnlewis.betechtest.products.response.VariantPriceRangeDto;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class ProductMapperTest {

    @Test
    void shouldMapProductWasThenNow() {

        String productId = "productId";
        String title = "title";
        VariantPriceRangeDto variantPriceRangeDto = new VariantPriceRangeDto();
        variantPriceRangeDto.setDisplay(new Display("2.00", "1.00"));
        ReductionHistoryDto reductionHistory = new ReductionHistoryDto();
        reductionHistory.setDisplay(new Display("15.00", "10.00"));
        reductionHistory.setChronology(0);
        ReductionHistoryDto reductionHistory1 = new ReductionHistoryDto();
        reductionHistory1.setDisplay(new Display("10.00", "5.00"));
        reductionHistory1.setChronology(1);
        variantPriceRangeDto.setReductionHistory(List.of(reductionHistory1, reductionHistory));
        ColorSwatchDto colorSwatchDto = new ColorSwatchDto();
        colorSwatchDto.setBasicColor("Blue");
        colorSwatchDto.setSkuId("skuId");
        ColorSwatchDto colorSwatchDto1 = new ColorSwatchDto();
        colorSwatchDto1.setBasicColor("Red");
        colorSwatchDto1.setSkuId("skuId1");
        List<ColorSwatchDto> colorSwatchDtos = List.of(colorSwatchDto, colorSwatchDto1);

        ProductDto productDto = new ProductDto();
        productDto.setProductId(productId);
        productDto.setTitle(title);
        productDto.setColorSwatches(colorSwatchDtos);
        productDto.setVariantPriceRange(variantPriceRangeDto);

        Product product = new Product();
        product.setProductId(productId);
        product.setTitle(title);
        ColorSwatch colorSwatch = new ColorSwatch("Blue", "skuId", null);
        ColorSwatch colorSwatch1 = new ColorSwatch("Red", "skuId1",null);
        product.setColorSwatches(List.of(colorSwatch, colorSwatch1));
        product.setPrice(new Price("15.00", "10.00", "2.00"));

        ProductMapper productMapper = ProductMapper.INSTANCE;
        Product result = productMapper.toProduct(productDto);

        assertEquals(product, result);
    }

    @Test
    void shouldMapProductWasNow() {

        String productId = "productId";
        String title = "title";
        VariantPriceRangeDto variantPriceRangeDto = new VariantPriceRangeDto();
        variantPriceRangeDto.setDisplay(new Display("2.00", "1.00"));
        ReductionHistoryDto reductionHistory = new ReductionHistoryDto();
        reductionHistory.setDisplay(new Display("15.00", "10.00"));
        reductionHistory.setChronology(0);
        variantPriceRangeDto.setReductionHistory(List.of(reductionHistory));
        ColorSwatchDto colorSwatchDto = new ColorSwatchDto();
        colorSwatchDto.setBasicColor("Blue");
        colorSwatchDto.setSkuId("skuId");
        ColorSwatchDto colorSwatchDto1 = new ColorSwatchDto();
        colorSwatchDto1.setBasicColor("Red");
        colorSwatchDto1.setSkuId("skuId1");
        List<ColorSwatchDto> colorSwatchDtos = List.of(colorSwatchDto, colorSwatchDto1);

        ProductDto productDto = new ProductDto();
        productDto.setProductId(productId);
        productDto.setTitle(title);
        productDto.setColorSwatches(colorSwatchDtos);
        productDto.setVariantPriceRange(variantPriceRangeDto);

        Product product = new Product();
        product.setProductId(productId);
        product.setTitle(title);
        ColorSwatch colorSwatch = new ColorSwatch("Blue", "skuId", null);
        ColorSwatch colorSwatch1 = new ColorSwatch("Red", "skuId1", null);
        product.setColorSwatches(List.of(colorSwatch, colorSwatch1));
        product.setPrice(new Price("15.00", null, "2.00"));

        ProductMapper productMapper = ProductMapper.INSTANCE;
        Product result = productMapper.toProduct(productDto);

        assertEquals(product, result);
    }

    @Test
    void shouldMapProductNow() {

        String productId = "productId";
        String title = "title";
        VariantPriceRangeDto variantPriceRangeDto = new VariantPriceRangeDto();
        variantPriceRangeDto.setDisplay(new Display("2.00", "1.00"));
        ColorSwatchDto colorSwatchDto = new ColorSwatchDto();
        colorSwatchDto.setBasicColor("Blue");
        colorSwatchDto.setSkuId("skuId");
        ColorSwatchDto colorSwatchDto1 = new ColorSwatchDto();
        colorSwatchDto1.setBasicColor("Red");
        colorSwatchDto1.setSkuId("skuId1");
        List<ColorSwatchDto> colorSwatchDtos = List.of(colorSwatchDto, colorSwatchDto1);

        ProductDto productDto = new ProductDto();
        productDto.setProductId(productId);
        productDto.setTitle(title);
        productDto.setColorSwatches(colorSwatchDtos);
        productDto.setVariantPriceRange(variantPriceRangeDto);

        Product product = new Product();
        product.setProductId(productId);
        product.setTitle(title);
        ColorSwatch colorSwatch = new ColorSwatch("Blue", "skuId", null);
        ColorSwatch colorSwatch1 = new ColorSwatch("Red", "skuId1", null);
        product.setColorSwatches(List.of(colorSwatch, colorSwatch1));
        product.setPrice(new Price(null, null, "2.00"));

        ProductMapper productMapper = ProductMapper.INSTANCE;
        Product result = productMapper.toProduct(productDto);

        assertEquals(product, result);
    }

    @Test
    void shouldProvideNullResponseForEmptyObjectWhenMappingToPrice() {
        VariantPriceRangeDto variantPriceRangeDto = new VariantPriceRangeDto();
        ProductMapper productMapper = ProductMapper.INSTANCE;
        assertNull(productMapper.toPrice(variantPriceRangeDto));
    }

    @Test
    void shouldProvideNullResponseForEmptyObjectWhenMappingToProduct() {
        ProductMapper productMapper = ProductMapper.INSTANCE;
        assertNull(productMapper.toProduct(null));
    }

}
