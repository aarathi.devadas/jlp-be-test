package com.johnlewis.betechtest.products.utils;

public class Constants {

	public static final int INT_ZERO = 0;
	public static final String DEFAULT_LABEL_TYPE = "ShowWasNow";
	public static final String SHOW_WAS_THEN_NOW = "ShowWasThenNow";
	public static final String SHOW_WAS_NOW = "ShowWasNow";
	public static final String SHOW_PERC_DISCOUNT = "ShowPercDscount";
	public static final String EMPTY_STR = "";
	public static final String POUND = "£";
}
