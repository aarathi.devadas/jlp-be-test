package com.johnlewis.betechtest.products.response;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)

public class ColorSwatch {
    private String color;
    private String skuId;
    private String rgbColor;
}
